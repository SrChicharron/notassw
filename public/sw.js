importScripts('js/sw-utils.js')

//Crear las variables de cache
const CACHE_DYNAMIC = 'dynamic-v1' //Para los archivos que se van a descargar
const CACHE_STATIC = 'static-v1' //App shell
const CACHE_INMUTABLE = 'inmutable-v1'// CDN de terceros. LIBRERIAS

const limpiarCache = (cacheName, numberItem) => {
caches.open(cacheName)
.then(cache => {
cache.keys()
.then(keys => {
if (keys.length > numberItem) {
cache.delete(keys[0])
.then(limpiarCache(cacheName, numberItem))
}
})
})

}
self.addEventListener('install', function (event) {

const cahePromise = caches.open(CACHE_STATIC).then(function (cache) {

return cache.addAll([

'/',
'/index.html',
'/js/app.js',
'/js/sw-utils.js',
'/sw.js',
'static/js/bundle.js',
'/pages/offline.html'

])
})
const caheInmutable = caches.open(CACHE_INMUTABLE).then(function (cache) {

return cache.addAll([

'https://fonts.googleapis.com/css2?family=Inter:wght@300&family=Roboto:wght@100&display=swap&#39',

])
})
event.waitUntil(Promise.all([cahePromise, caheInmutable]))
})

/**self.addEventListener('fetch', function (event) {
    //Cache with network fallback
    const respuesta = caches.match(event.request)
    .then(response => {
    if (response) return response
    //Si no existe el archivo lo descarga de la web
    return fetch(event.request)
    .then(newResponse => {
    
    caches.open(CACHE_DYNAMIC)
    .then(cache => {
    cache.put(event.request, newResponse)
    limpiarCache(CACHE_DYNAMIC, 20)
    })
    return newResponse.clone()
    })//ToDo 2 Manejo de errores
    .catch(err => {
    if (event.request.headers.get('accept').includes('text/html')) {
    return caches.match('/pages/offline.html')
    }
    })
    })
    event.respondWith(respuesta)
    })
    
    self.addEventListener('activate', function (event) {
        const respuesta = caches.keys().then (
            keys => {
                keys.forEach(key => {
                    if(key !== CACHE_STATIC && key.includes('static')) {
                        return caches.delete(key)
                    }}
                )
            }
        )
    })
**/

self.addEventListener('fetch', function (event) {
    const respuesta = caches.match(event.request)
    .then(res => {
        if(res){
            return res
        }else{
            return fetch(event.request).then(newRes => {
                return actualizaCacheDinamico(CACHE_DYNAMIC,event.request,newRes)
            })
        }
    }
    )
    event.respondWith(respuesta)
})