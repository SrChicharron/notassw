// src/components/NoteModal.js
import React, { useState } from 'react';
import Modal from 'react-modal';
import axios from 'axios';

Modal.setAppElement('#root');

const NoteModal = ({ isOpen, onRequestClose }) => {
  const [formData, setFormData] = useState({
    title: '',
    text: '',
    noteDone: false,
  });

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleSubmit = () => {
    // Realiza una solicitud POST a la API para agregar una nueva nota
    axios.post('http://localhost:3000/api/noteAdd', formData)
      .then(() => {
        // Limpia el formulario y cierra el modal después de agregar la nota
        setFormData({ title: '', text: '', noteDone: false });
        onRequestClose();
      })
      .catch((error) => {
        console.error('Error al agregar la nota:', error);
      });
  };

  return (
    <Modal
      isOpen={isOpen}
      onRequestClose={onRequestClose}
      contentLabel="Agregar Nota"
    >
      <h2>Agregar Nota</h2>
      <form onSubmit={handleSubmit}>
        <div>
          <label htmlFor="title">Título:</label>
          <input
            type="text"
            id="title"
            name="title"
            value={formData.title}
            onChange={handleInputChange}
            required
          />
        </div>
        <div>
          <label htmlFor="text">Texto:</label>
          <textarea
            id="text"
            name="text"
            value={formData.text}
            onChange={handleInputChange}
            required
          />
        </div>
        <div>
          <label htmlFor="noteDone">¿Hecho?</label>
          <input
            type="checkbox"
            id="noteDone"
            name="noteDone"
            checked={formData.noteDone}
            onChange={() => setFormData({ ...formData, noteDone: !formData.noteDone })}
          />
        </div>
        <div>
          <button type="submit">Agregar</button>
        </div>
      </form>
    </Modal>
  );
};

export default NoteModal;
